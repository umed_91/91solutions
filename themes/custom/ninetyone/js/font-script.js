(function($) {
  Drupal.behaviors.ninetyone = {
    attach: function (context, settings) {
      //OWL CAROUSEL TESTIMONIAL
      $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          dots:true,
          dotsEach:true,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      });
      
    }
  }
})(jQuery);
jQuery(window).on('load', function () {
    jQuery('#status').fadeOut(); // will first fade out the loading animation
    jQuery('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    jQuery('body').delay(350).css({'overflow':'visible'});
	
	//Focus username on login
	if(jQuery("#user-login-form").length > 0) {
		jQuery("[id^=edit-name]").focus();
	}
	
});
