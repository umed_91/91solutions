(function($) {
  Drupal.behaviors.ninetyone = {
    attach: function (context, settings) {
      $("html").niceScroll({
        cursorcolor:"#f74d65",
        scrollspeed :"100",
        cursorborder:"5px solid #f74d65",
        horizrailenabled: "false",
        cursorborderradius: "0px"
      });
      
	  jQuery('.tryus, .usr-name').one("mouseover",(function() {
	    if (jQuery(this).hasClass('animated')) {
			console.log('came');
            jQuery(this).removeClass('animated');
            jQuery(this).removeAttr('style');
            new WOW().init();
			return false;
        }
	  }));
    }
  }
  
})(jQuery);
jQuery(window).on('load', function () {  
   //Active link menu
  if(jQuery("#block-ninetyone-main-menu").length > 0) {
		  jQuery("#block-ninetyone-main-menu ul li a").on('click',function(e) {
			  //Remove all other instance of 'active-link'
			  jQuery('a').removeClass('active-link');
			  jQuery(this).addClass('active-link');
			  //scroll to focus on element
			  var linkHref = jQuery(this).attr("href"); 
			  linkHref = linkHref.substring(linkHref.indexOf("#"));
			  if(jQuery(linkHref).length > 0) {
			    e.preventDefault();
				jQuery('html, body').animate({
					scrollTop: jQuery(linkHref).offset().top - parseInt(100)
				}, 1000);
			  }
			  return;
		  });
  }

});