<?php

/* themes/custom/ninetyone/templates/page.html.twig */
class __TwigTemplate_64b43908f15b24c686feb27d93a73dd098f0c480fb99709b7c328917ebe89304 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 60, "if" => 62, "block" => 63);
        $filters = array("t" => 95, "clean_class" => 68);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('t', 'clean_class'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 59
        echo "
";
        // line 60
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 62
        if (($this->getAttribute(($context["page"] ?? null), "navigation", array()) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()))) {
            // line 63
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 72
        echo "\t

";
        // line 75
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 76
            echo "\t";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
            echo "
  ";
        }
        // line 78
        echo "
 <!-- Body -->
<div class=\"main-containers\">
\t<header>
\t  <div class=\"section_overlay\">
\t  ";
        // line 83
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 84
            echo "\t\t  <nav class=\"navbar navbar-default\" data-spy=\"affix\" data-offset-top=\"560\">
\t  ";
        } else {
            // line 86
            echo "\t      <nav class=\"navbar navbar-default\" data-spy=\"affix\" data-offset-top=\"0\">
\t  ";
        }
        // line 88
        echo "\t\t\t<div class=\"container\" style=\"width:93%;\">
\t\t\t  <!-- Brand and toggle get grouped for better mobile display -->
\t\t\t  <div class=\"navbar-header\">
\t\t\t\t";
        // line 91
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation", array()), "html", null, true));
        echo "
\t\t\t\t";
        // line 93
        echo "\t\t\t\t";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 94
            echo "\t\t\t\t  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
\t\t\t\t\t<span class=\"sr-only\">";
            // line 95
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t  </button>
\t\t\t\t";
        }
        // line 101
        echo "\t\t\t  </div>

\t\t\t  <!-- Collect the nav links, forms, and other content for toggling -->
\t\t\t  ";
        // line 104
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 105
            echo "\t\t\t\t<div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
\t\t\t\t  ";
            // line 106
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()), "html", null, true));
            echo "
\t\t\t\t</div>
\t\t\t  ";
        }
        // line 109
        echo "\t\t\t</div><!-- /.container -->
\t\t\t</nav> 
\t\t\t";
        // line 112
        echo "\t\t   ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 113
            echo "\t\t\t";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 118
            echo "\t\t\t";
        }
        // line 119
        echo "\t\t\t
\t\t  ";
        // line 121
        echo "\t\t  ";
        if (($context["breadcrumb"] ?? null)) {
            // line 122
            echo "\t\t\t";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 125
            echo "\t\t  ";
        }
        // line 126
        echo "
\t\t  ";
        // line 128
        echo "\t\t  ";
        if (($context["action_links"] ?? null)) {
            // line 129
            echo "\t\t\t";
            $this->displayBlock('action_links', $context, $blocks);
            // line 132
            echo "\t\t  ";
        }
        // line 133
        echo "\t\t  
\t\t\t";
        // line 134
        echo "        
\t\t\t";
        // line 135
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 136
            echo "\t\t\t  ";
            $this->displayBlock('help', $context, $blocks);
            // line 139
            echo "\t\t\t  ";
        }
        // line 140
        echo "\t  </div>
\t</header>
\t
\t<section class=\"inner-content\">
\t\t";
        // line 145
        echo "\t\t\t  ";
        if ($this->getAttribute(($context["page"] ?? null), "content", array())) {
            // line 146
            echo "\t\t\t\t";
            $this->displayBlock('content', $context, $blocks);
            // line 149
            echo "\t\t\t  ";
        }
        // line 150
        echo "\t</section>
    <!-- Footer -->
\t<footer class=\"\" role=\"contentinfo\">
      <div class=\"container\">
\t    ";
        // line 154
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 155
            echo "           ";
            $this->displayBlock('footer', $context, $blocks);
            // line 164
            echo "        ";
        }
        // line 165
        echo "\t\t
\t\t";
        // line 166
        if ($this->getAttribute(($context["page"] ?? null), "footer_bottom", array())) {
            // line 167
            echo "        <div class=\"row\">
          <div class=\"col-md-12 text-center\">
            <div class=\"footer_logo wow fadeInUp animated\">
              ";
            // line 170
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom", array()), "html", null, true));
            echo "
            </div>
\t\t  </div>
\t\t</div>
\t\t";
        }
        // line 175
        echo "      </div>
    </footer>
</div> <!--main-container ends -->";
    }

    // line 63
    public function block_navbar($context, array $blocks = array())
    {
        // line 64
        echo "    ";
        // line 65
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 67
($context["theme"] ?? null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 68
($context["theme"] ?? null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "navbar_position", array())))) : (($context["container"] ?? null))));
        // line 71
        echo "  ";
    }

    // line 113
    public function block_highlighted($context, array $blocks = array())
    {
        // line 114
        echo "\t\t\t  <!--<div class=\"highlighted\">-->
\t\t\t  ";
        // line 116
        echo "\t\t\t  <!--</div>-->
\t\t\t";
    }

    // line 122
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 123
        echo "\t\t\t  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["breadcrumb"] ?? null), "html", null, true));
        echo "
\t\t\t";
    }

    // line 129
    public function block_action_links($context, array $blocks = array())
    {
        // line 130
        echo "\t\t\t  <ul class=\"action-links\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["action_links"] ?? null), "html", null, true));
        echo "</ul>
\t\t\t";
    }

    // line 136
    public function block_help($context, array $blocks = array())
    {
        echo "\t  
\t\t\t\t";
        // line 137
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
\t\t\t  ";
    }

    // line 146
    public function block_content($context, array $blocks = array())
    {
        // line 147
        echo "\t\t\t\t\t";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
        echo "
\t\t\t\t";
    }

    // line 155
    public function block_footer($context, array $blocks = array())
    {
        // line 156
        echo "            <div class=\"row\">
                <div class=\"col-md-12 text-center\">
                    <div class=\"footer_logo wow fadeInUp animated\">
\t\t\t\t\t\t";
        // line 159
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
                    </div>
                </div>
            </div>
\t      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/ninetyone/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 159,  303 => 156,  300 => 155,  293 => 147,  290 => 146,  284 => 137,  279 => 136,  272 => 130,  269 => 129,  262 => 123,  259 => 122,  254 => 116,  251 => 114,  248 => 113,  244 => 71,  242 => 68,  241 => 67,  240 => 65,  238 => 64,  235 => 63,  229 => 175,  221 => 170,  216 => 167,  214 => 166,  211 => 165,  208 => 164,  205 => 155,  203 => 154,  197 => 150,  194 => 149,  191 => 146,  188 => 145,  182 => 140,  179 => 139,  176 => 136,  174 => 135,  171 => 134,  168 => 133,  165 => 132,  162 => 129,  159 => 128,  156 => 126,  153 => 125,  150 => 122,  147 => 121,  144 => 119,  141 => 118,  138 => 113,  135 => 112,  131 => 109,  125 => 106,  122 => 105,  120 => 104,  115 => 101,  106 => 95,  103 => 94,  100 => 93,  96 => 91,  91 => 88,  87 => 86,  83 => 84,  81 => 83,  74 => 78,  68 => 76,  65 => 75,  61 => 72,  57 => 63,  55 => 62,  53 => 60,  50 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/ninetyone/templates/page.html.twig", "/home/vmgco7tf/public_html/91solutions.com/themes/custom/ninetyone/templates/page.html.twig");
    }
}
