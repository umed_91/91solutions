<?php

/* themes/custom/ninetyone/templates/page--front.html.twig */
class __TwigTemplate_7db9544b6edea014470c5f0d0e5e6f623f41d2270f06101ba7db7a5511e5eff7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'contactblock' => array($this, 'block_contactblock'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 63, "if" => 65, "block" => 66);
        $filters = array("t" => 94, "clean_class" => 71, "without" => 146);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('t', 'clean_class', 'without'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 59
        echo "<!-- Preloader -->
    <div id=\"preloader\">
        <div id=\"status\">&nbsp;</div>
    </div>
";
        // line 63
        $context["container"] = (($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 65
        if (($this->getAttribute(($context["page"] ?? null), "navigation", array()) || $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()))) {
            // line 66
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 75
        echo "\t

";
        // line 78
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 79
            echo "\t";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
            echo "
  ";
        }
        // line 81
        echo "
 <!-- Body -->
<div class=\"main-container\">
\t<header id=\"HOME\">
\t  <div class=\"section_overlay\">
\t\t  <nav class=\"navbar navbar-default \" data-spy=\"affix\" data-offset-top=\"560\">
\t\t\t<div class=\"container\" style=\"width:93%;\">
\t\t\t  <!-- Brand and toggle get grouped for better mobile display -->
\t\t\t  <div class=\"navbar-header\">
\t\t\t\t";
        // line 90
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation", array()), "html", null, true));
        echo "
\t\t\t\t";
        // line 92
        echo "\t\t\t\t";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 93
            echo "\t\t\t\t  <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
\t\t\t\t\t<span class=\"sr-only\">";
            // line 94
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t\t<span class=\"icon-bar\"></span>
\t\t\t\t  </button>
\t\t\t\t";
        }
        // line 100
        echo "\t\t\t  </div>

\t\t\t  <!-- Collect the nav links, forms, and other content for toggling -->
\t\t\t  ";
        // line 103
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array())) {
            // line 104
            echo "\t\t\t\t<div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
\t\t\t\t  ";
            // line 105
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "navigation_collapsible", array()), "html", null, true));
            echo "
\t\t\t\t</div>
\t\t\t  ";
        }
        // line 108
        echo "\t\t\t</div><!-- /.container -->
\t\t\t</nav> 
\t\t\t";
        // line 111
        echo "\t\t   ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 112
            echo "\t\t\t";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 117
            echo "\t\t\t";
        }
        // line 118
        echo "\t\t\t
\t\t  ";
        // line 120
        echo "\t\t  ";
        if (($context["breadcrumb"] ?? null)) {
            // line 121
            echo "\t\t\t";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 124
            echo "\t\t  ";
        }
        // line 125
        echo "
\t\t  ";
        // line 127
        echo "\t\t  ";
        if (($context["action_links"] ?? null)) {
            // line 128
            echo "\t\t\t";
            $this->displayBlock('action_links', $context, $blocks);
            // line 131
            echo "\t\t  ";
        }
        // line 132
        echo "\t\t  
\t\t\t";
        // line 133
        echo "        
\t\t\t";
        // line 134
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 135
            echo "\t\t\t  ";
            $this->displayBlock('help', $context, $blocks);
            // line 138
            echo "\t\t\t  ";
        }
        // line 139
        echo "\t  </div>
\t</header>
\t
\t<section class=\"content\">
\t\t";
        // line 144
        echo "\t\t\t  ";
        if ($this->getAttribute(($context["page"] ?? null), "content", array())) {
            // line 145
            echo "\t\t\t\t";
            $this->displayBlock('content', $context, $blocks);
            // line 148
            echo "\t\t\t  ";
        }
        // line 149
        echo "
\t\t <div class=\"container\" id=\"contact\">
\t\t\t  <div class=\"row\">
\t\t\t\t  <div class=\"col-md-12 text-center\">
\t\t\t\t\t  <div class=\"contact_title  wow fadeInUp animated\">
\t\t\t\t\t\t  <h1>How can we help you?</h1>
\t\t\t\t\t\t  <img src=\"";
        // line 155
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (($context["base_path"] ?? null) . ($context["directory"] ?? null)), "html", null, true));
        echo "/images/shape.png\" alt=\"\">
\t\t\t\t\t\t  <p></p>
\t\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  </div>
\t\t  </div>
\t\t  <div class=\"container\">
\t\t\t  <div class=\"row\">
\t\t\t\t  <div class=\"col-md-3  wow fadeInLeft animated\">
\t\t\t\t\t  <div class=\"single_contact_info\">
\t\t\t\t\t\t  <h2><i class=\"glyphicon glyphicon-earphone\"></i> Contact Us:</h2>
\t\t\t\t\t\t  <p>011-40392191</p>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"single_contact_info\">
\t\t\t\t\t\t  <h2><i class=\"glyphicon glyphicon-pencil\"></i> Write Us:</h2>
\t\t\t\t\t\t  <p>info@91solutions.com</p>
\t\t\t\t\t  </div>
\t\t\t\t\t  <div class=\"single_contact_info\">
\t\t\t\t\t\t  <h2><i class=\"fa fa-building\" aria-hidden=\"true\"></i> Visit Us:</h2>
\t\t\t\t\t\t  <p>Plot no. 14, Lower Ground Floor, KH No 28/7/1, Kapashera Najafgarh Road, New Delhi - 110037</p>
\t\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t\t  <div class=\"col-md-9  wow fadeInRight animated\">
\t\t\t\t\t";
        // line 178
        $this->displayBlock('contactblock', $context, $blocks);
        // line 181
        echo "\t\t\t\t  </div>
\t\t\t  </div>
\t\t  </div>
\t\t  <div class=\"container\">
\t\t\t  <div class=\"row\">
\t\t\t\t  <div class=\"col-md-12 text-center\">
\t\t\t\t\t  <div class=\"work-with   wow fadeInUp animated\">
\t\t\t\t\t\t  <h3>looking forward to hearing from you!</h3>
\t\t\t\t\t  </div>
\t\t\t\t  </div>
\t\t\t  </div>
\t\t  </div>
\t\t</section>
    <!-- Footer -->
\t<footer class=\"\" role=\"contentinfo\">
      <div class=\"container\">
\t    ";
        // line 197
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 198
            echo "           ";
            $this->displayBlock('footer', $context, $blocks);
            // line 207
            echo "        ";
        }
        // line 208
        echo "\t\t
\t\t";
        // line 209
        if ($this->getAttribute(($context["page"] ?? null), "footer_bottom", array())) {
            // line 210
            echo "        <div class=\"row\">
          <div class=\"col-md-12 text-center\">
            <div class=\"footer_logo wow fadeInUp animated\">
              ";
            // line 213
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom", array()), "html", null, true));
            echo "
            </div>
\t\t  </div>
\t\t</div>
\t\t";
        }
        // line 218
        echo "      </div>
    </footer>
</div> <!--main-container ends -->";
    }

    // line 66
    public function block_navbar($context, array $blocks = array())
    {
        // line 67
        echo "    ";
        // line 68
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 70
($context["theme"] ?? null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 71
($context["theme"] ?? null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute(($context["theme"] ?? null), "settings", array()), "navbar_position", array())))) : (($context["container"] ?? null))));
        // line 74
        echo "  ";
    }

    // line 112
    public function block_highlighted($context, array $blocks = array())
    {
        // line 113
        echo "\t\t\t  <!--<div class=\"highlighted\">-->
\t\t\t  ";
        // line 115
        echo "\t\t\t  <!--</div>-->
\t\t\t";
    }

    // line 121
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 122
        echo "\t\t\t  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["breadcrumb"] ?? null), "html", null, true));
        echo "
\t\t\t";
    }

    // line 128
    public function block_action_links($context, array $blocks = array())
    {
        // line 129
        echo "\t\t\t  <ul class=\"action-links\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["action_links"] ?? null), "html", null, true));
        echo "</ul>
\t\t\t";
    }

    // line 135
    public function block_help($context, array $blocks = array())
    {
        echo "\t  
\t\t\t\t";
        // line 136
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
\t\t\t  ";
    }

    // line 145
    public function block_content($context, array $blocks = array())
    {
        // line 146
        echo "\t\t\t\t\t";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_without($this->getAttribute(($context["page"] ?? null), "content", array()), "contactblock"), "html", null, true));
        echo "
\t\t\t\t";
    }

    // line 178
    public function block_contactblock($context, array $blocks = array())
    {
        // line 179
        echo "\t\t\t\t\t\t  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute(($context["page"] ?? null), "content", array()), "contactblock", array()), "html", null, true));
        echo "
\t\t\t\t\t";
    }

    // line 198
    public function block_footer($context, array $blocks = array())
    {
        // line 199
        echo "            <div class=\"row\">
                <div class=\"col-md-12 text-center\">
                    <div class=\"footer_logo wow fadeInUp animated\">
\t\t\t\t\t\t";
        // line 202
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
                    </div>
                </div>
            </div>
\t      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/ninetyone/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 202,  354 => 199,  351 => 198,  344 => 179,  341 => 178,  334 => 146,  331 => 145,  325 => 136,  320 => 135,  313 => 129,  310 => 128,  303 => 122,  300 => 121,  295 => 115,  292 => 113,  289 => 112,  285 => 74,  283 => 71,  282 => 70,  281 => 68,  279 => 67,  276 => 66,  270 => 218,  262 => 213,  257 => 210,  255 => 209,  252 => 208,  249 => 207,  246 => 198,  244 => 197,  226 => 181,  224 => 178,  198 => 155,  190 => 149,  187 => 148,  184 => 145,  181 => 144,  175 => 139,  172 => 138,  169 => 135,  167 => 134,  164 => 133,  161 => 132,  158 => 131,  155 => 128,  152 => 127,  149 => 125,  146 => 124,  143 => 121,  140 => 120,  137 => 118,  134 => 117,  131 => 112,  128 => 111,  124 => 108,  118 => 105,  115 => 104,  113 => 103,  108 => 100,  99 => 94,  96 => 93,  93 => 92,  89 => 90,  78 => 81,  72 => 79,  69 => 78,  65 => 75,  61 => 66,  59 => 65,  57 => 63,  51 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/ninetyone/templates/page--front.html.twig", "/home/vmgco7tf/public_html/91solutions.com/themes/custom/ninetyone/templates/page--front.html.twig");
    }
}
