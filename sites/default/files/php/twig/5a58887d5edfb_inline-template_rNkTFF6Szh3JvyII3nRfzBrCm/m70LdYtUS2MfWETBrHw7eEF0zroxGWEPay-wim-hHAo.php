<?php

/* {# inline_template_start #}<div class='user-pattern-details'>
<div class = "usr-pic">{{ user_picture }}</div>
<div class = "usr-name wow swing animated">Hi, I am {{ name }} {{ field_usr_slogan }}</div>
<div class = "usr-name wow fade-in animated">{{ field_ofc_mail }}</div>
<div class="tryus wow tada animated"><a href="#contact">Know More</a></div>
</div>
<div class="overlay"></div>
<div class='user-pattern-bg'>{{ field_my_pattern }}</div>
 */
class __TwigTemplate_976d624dfc511d6d1a47a7787e33cd0eba1cccc9e844e6e2aea8a31a319442c6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class='user-pattern-details'>
<div class = \"usr-pic\">";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["user_picture"] ?? null), "html", null, true));
        echo "</div>
<div class = \"usr-name wow swing animated\">Hi, I am ";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["name"] ?? null), "html", null, true));
        echo " ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_usr_slogan"] ?? null), "html", null, true));
        echo "</div>
<div class = \"usr-name wow fade-in animated\">";
        // line 4
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_ofc_mail"] ?? null), "html", null, true));
        echo "</div>
<div class=\"tryus wow tada animated\"><a href=\"#contact\">Know More</a></div>
</div>
<div class=\"overlay\"></div>
<div class='user-pattern-bg'>";
        // line 8
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["field_my_pattern"] ?? null), "html", null, true));
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class='user-pattern-details'>
<div class = \"usr-pic\">{{ user_picture }}</div>
<div class = \"usr-name wow swing animated\">Hi, I am {{ name }} {{ field_usr_slogan }}</div>
<div class = \"usr-name wow fade-in animated\">{{ field_ofc_mail }}</div>
<div class=\"tryus wow tada animated\"><a href=\"#contact\">Know More</a></div>
</div>
<div class=\"overlay\"></div>
<div class='user-pattern-bg'>{{ field_my_pattern }}</div>
";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 8,  64 => 4,  58 => 3,  54 => 2,  51 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class='user-pattern-details'>
<div class = \"usr-pic\">{{ user_picture }}</div>
<div class = \"usr-name wow swing animated\">Hi, I am {{ name }} {{ field_usr_slogan }}</div>
<div class = \"usr-name wow fade-in animated\">{{ field_ofc_mail }}</div>
<div class=\"tryus wow tada animated\"><a href=\"#contact\">Know More</a></div>
</div>
<div class=\"overlay\"></div>
<div class='user-pattern-bg'>{{ field_my_pattern }}</div>
", "");
    }
}
