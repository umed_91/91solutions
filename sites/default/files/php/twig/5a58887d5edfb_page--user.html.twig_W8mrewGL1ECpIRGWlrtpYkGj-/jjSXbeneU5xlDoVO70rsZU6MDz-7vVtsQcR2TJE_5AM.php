<?php

/* themes/custom/ninetyone/templates/page--user.html.twig */
class __TwigTemplate_fa39652e79ed7d141753524e013599e5cf3b03f17ced7aa0ed398273fc8c99e0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 3, "block" => 14);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if', 'block'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "
";
        // line 3
        echo "  ";
        if ($this->getAttribute(($context["page"] ?? null), "header", array())) {
            // line 4
            echo "\t";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "header", array()), "html", null, true));
            echo "
  ";
        }
        // line 6
        echo "
 <!-- Body -->
<div class=\"main-container\">
\t<header>
\t  <div class=\"section_overlay test\">
\t\t  
\t\t\t";
        // line 13
        echo "\t\t   ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", array())) {
            // line 14
            echo "\t\t\t";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 19
            echo "\t\t\t";
        }
        // line 20
        echo "\t\t\t
\t\t  ";
        // line 22
        echo "\t\t  ";
        if (($context["breadcrumb"] ?? null)) {
            // line 23
            echo "\t\t\t";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 26
            echo "\t\t  ";
        }
        // line 27
        echo "
\t\t\t";
        // line 28
        echo "        
\t\t\t";
        // line 29
        if ($this->getAttribute(($context["page"] ?? null), "help", array())) {
            // line 30
            echo "\t\t\t  ";
            $this->displayBlock('help', $context, $blocks);
            // line 33
            echo "\t\t\t  ";
        }
        // line 34
        echo "\t  </div>
\t</header>
\t
\t<section class=\"inner-content\">
\t\t";
        // line 39
        echo "\t\t\t  ";
        if ($this->getAttribute(($context["page"] ?? null), "content", array())) {
            // line 40
            echo "\t\t\t\t";
            $this->displayBlock('content', $context, $blocks);
            // line 43
            echo "\t\t\t  ";
        }
        // line 44
        echo "\t</section>
    <!-- Footer -->
\t<footer class=\"\" role=\"contentinfo\">
      <div class=\"container\">
\t    ";
        // line 48
        if ($this->getAttribute(($context["page"] ?? null), "footer", array())) {
            // line 49
            echo "           ";
            $this->displayBlock('footer', $context, $blocks);
            // line 58
            echo "        ";
        }
        // line 59
        echo "\t\t
\t\t";
        // line 60
        if ($this->getAttribute(($context["page"] ?? null), "footer_bottom", array())) {
            // line 61
            echo "        <div class=\"row\">
          <div class=\"col-md-12 text-center\">
            <div class=\"footer_logo wow fadeInUp animated\">
              ";
            // line 64
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer_bottom", array()), "html", null, true));
            echo "
            </div>
\t\t  </div>
\t\t</div>
\t\t";
        }
        // line 69
        echo "      </div>
    </footer>
</div> <!--main-container ends -->";
    }

    // line 14
    public function block_highlighted($context, array $blocks = array())
    {
        // line 15
        echo "\t\t\t  <div class=\"highlighted\">
\t\t\t   ";
        // line 16
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "highlighted", array()), "html", null, true));
        echo "
\t\t\t  </div>
\t\t\t";
    }

    // line 23
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 24
        echo "\t\t\t  ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, ($context["breadcrumb"] ?? null), "html", null, true));
        echo "
\t\t\t";
    }

    // line 30
    public function block_help($context, array $blocks = array())
    {
        echo "\t  
\t\t\t\t";
        // line 31
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "help", array()), "html", null, true));
        echo "
\t\t\t  ";
    }

    // line 40
    public function block_content($context, array $blocks = array())
    {
        // line 41
        echo "\t\t\t\t\t";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "content", array()), "html", null, true));
        echo "
\t\t\t\t";
    }

    // line 49
    public function block_footer($context, array $blocks = array())
    {
        // line 50
        echo "            <div class=\"row\">
                <div class=\"col-md-12 text-center\">
                    <div class=\"footer_logo wow fadeInUp animated\">
\t\t\t\t\t\t";
        // line 53
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute(($context["page"] ?? null), "footer", array()), "html", null, true));
        echo "
                    </div>
                </div>
            </div>
\t      ";
    }

    public function getTemplateName()
    {
        return "themes/custom/ninetyone/templates/page--user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  208 => 53,  203 => 50,  200 => 49,  193 => 41,  190 => 40,  184 => 31,  179 => 30,  172 => 24,  169 => 23,  162 => 16,  159 => 15,  156 => 14,  150 => 69,  142 => 64,  137 => 61,  135 => 60,  132 => 59,  129 => 58,  126 => 49,  124 => 48,  118 => 44,  115 => 43,  112 => 40,  109 => 39,  103 => 34,  100 => 33,  97 => 30,  95 => 29,  92 => 28,  89 => 27,  86 => 26,  83 => 23,  80 => 22,  77 => 20,  74 => 19,  71 => 14,  68 => 13,  60 => 6,  54 => 4,  51 => 3,  48 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/custom/ninetyone/templates/page--user.html.twig", "/home/vmgco7tf/public_html/91solutions.com/themes/custom/ninetyone/templates/page--user.html.twig");
    }
}
